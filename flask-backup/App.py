from flask import Flask, render_template, request
import pymysql
import re

conexao = pymysql.connect(db='ApiPedro', user='root', passwd='1234', host='localhost')


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        detalhes = request.form
        CPF = detalhes['CPF']
        num_cpf_limpo= re.sub('[^0-9]', '', CPF)
        cursor = conexao.cursor()
        cursor.execute (f"SELECT Nome, DataNascimento FROM Dados WHERE CPF={num_cpf_limpo};")
        dados = cursor.fetchall()
        if len(dados) == 0:
            Nome="Não Encontrado"
            Data_nascimento=" Não Encontrado"
        else:
            for dado in dados:
                Nome=dado[0]
                Data_nascimento=dado[1]
            
        cursor.connection.commit()
        cursor.close()
        return render_template('index.html', Nome=Nome, DataNascimento=Data_nascimento)
    return render_template('index.html')


if __name__ == '__main__':
    app.run(port=7501)
