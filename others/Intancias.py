
padrao1 = [2,1,1]
padrao2 = [2,0,0]

PATTERN1 = 1
PATTERN2 = 2

instancia = [[1,1,1],[2,0,1],[2,0,3],[1,1,1],]


count_instance1 = {"instancia 1":1, "instancia 2":1, "instancia 3":2}
count_instance2 = {"instancia 1":1, "instancia 2":1, "instancia 3":1}

def check_patternOLD(pattern, list_inst):
    for key in list_inst:
        print(key)

    for value in list_inst.values():
        print(value)

    for key, value in list_inst.items():
        print(key + " " + str(value))

    if pattern == 1:
        if list_inst == {"instancia 1":1, "instancia 2":1, "instancia 3":1}:
            return True
        else:
            return False
    
       
    if pattern == 2:
        if list_inst != {"instancia 1":1, "instancia 2":1, "instancia 3":1}:
            return True
        else:
            return False


def check_pattern(pattern, instancia_found):
    if len(instancia_found) > 1:
        is_pattern3 = any([v > 1 for k, v in instancia_found.items()])
        return (not is_pattern3) if (pattern == PATTERN1) else is_pattern3
    return False

print(check_pattern(PATTERN1,count_instance1)) #False
print(check_pattern(PATTERN1,count_instance2)) #True
print(check_pattern(PATTERN2,count_instance1)) #True
print(check_pattern(PATTERN2,count_instance2)) #False