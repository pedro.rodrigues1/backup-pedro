#http://pythonclub.com.br/introducao-classes-metodos-python-basico.html

class Pessoa:

    def __init__(self, nome):
        self.nome = nome

    def __str__(self):
        return self.nome

# regis = Pessoa('Regis')
# print(regis)


class Calculadora:

    def __init__(self, numa, numb):
        self.numa = numa
        self.numb = numb
    
    def soma(self):
        return self.numa + self.numb

    def subtrai(self):
        return self.numa - self.numb
    
    def divide(self):
        return self.numa / self.numb

    def multiplica(self):
        return self.numa * self.numb

# lol = Calculadora(128, 2)
# print(lol.soma(), lol.subtrai(), lol.divide(), lol.multiplica())

class Calculadora2:

    def soma(self, a, b):
        return a + b

    def subtrai(self, a, b):
        return a - b

    def multiplica(self, a, b):
        return a * b

    def divide(self, a, b):
        return a / b

# lol2 = Calculadora2()
# print(lol2.multiplica(2,5))
    
class MediaAluno:
    def __init__(self, nome, nota1, nota2, nota3, nota4):
        self.nome = nome
        self.nota1 = nota1
        self.nota2 = nota2
        self.nota3 = nota3
        self.nota4 = nota4
    
    def media(self):
        return (self.nota1 + self.nota2 + self.nota3 + self.nota4) / 4

    def resultado(self):
        return "Aprovado" if self.media() >= 6 else "Reprovado"

    def retorno(self):
        return f'O aluno {self.nome} tirou {self.media()}, portando foi {self.resultado()}'


pedro = MediaAluno('Pedro', 5, 7, 8, 3)
ana = MediaAluno('Ana', 5, 7, 8, 4)
print(pedro.retorno())
print(ana.retorno())



    
