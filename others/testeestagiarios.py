from datetime import timedelta, date, datetime
import re



def resposta2():
    arquivo = open('resposta2.txt', 'w')

    data_inicial = date(2019, 1, 1)
    data_final = date(2019, 12, 31)

    i=0
    periodo = data_inicial
    while periodo != data_final:
        periodo = data_inicial + timedelta(i)
        i+=1
        periodo_formatado = (periodo.strftime('%d/%m/%Y'))
        arquivo.write(str(periodo_formatado)+'\n')

    arquivo.close()

def resposta3():
    arquivo = open('resposta2.txt', 'r')
    arquivo2 = open('resposta3.txt', 'w')
    for linha in arquivo:
        linha = re.sub('\n','',linha)
        date = datetime.strptime(linha,'%d/%m/%Y').date()
        date = date.strftime('%d/%B/%Y')
        arquivo2.write(linha + ' - '+ date+'\n')

    arquivo.close()


def resposta4():
    arquivo = open('resposta3.txt', 'r')
    arquivo2 = open('resposta4.txt', 'w')
    for linha in arquivo:
        linha = re.sub('\n','',linha)
        linha = linha.split('-')
        data1 = linha[0].split('/')
        data2 = linha[1].split('/')
        data1final = data1[2]+'/'+data1[1]+'/'+data1[0]
        data2final = data2[2]+'/'+data2[1]+'/'+data2[0]
        arquivo2.write(data2final + ' ; ' + data1final + '\n')
        
    arquivo.close()


def main():
    resposta2()
    resposta3()
    resposta4()

if __name__ == "__main__":
    main()


       

