# https://wiki.python.org.br/ExerciciosClasses

"""
Classe Bola: Crie uma classe que modele uma bola:

    Atributos: Cor, circunferência, material
    Métodos: trocaCor e mostraCor 
"""
class Bola:
    def __init__(self, cor, circunferencia, material):
        self.__cor = cor
        self.__circunferencia = circunferencia
        self.__material = material

    def troca_cor(self, nova_cor):
        self.__cor = nova_cor

    def mostracor(self):
        return self.__cor


"""
Classe Quadrado: Crie uma classe que modele um quadrado:

    Atributos: Tamanho do lado
    Métodos: Mudar valor do Lado, Retornar valor do Lado e calcular Área; 
"""
class Quadrado:
    def __init__(self, tamanho_lado):
        self.__tamanho_lado = tamanho_lado
    
    def muda_valor_lado(self, novo_valor_lado):
        self.__tamanho_lado = novo_valor_lado

    def retorna_area(self):
        area = self.__tamanho_lado ** 2
        return area


"""
Classe Retangulo: Crie uma classe que modele um retangulo:
    Atributos: LadoA, LadoB (ou base e altura, ou __base e Altura, a escolher)
    Métodos: Mudar valor dos lados, Retornar valor dos lados, calcular Área e calcular Perímetro;
    Crie um programa que utilize esta classe. Ele deve pedir ao usuário que informe as medidades de um local. 
    Depois, deve criar um objeto com as medidas e calcular a quantidade de pisos e de rodapés necessárias para o local. 
"""

class Retangulo:
    def __init__(self, base, altura):
        self.__base = base
        self.__altura = altura

    def muda_valor_lado(self, base, altura):
        self.__base = base
        self.__altura = altura

    def calcular_area(self):
        area = self.__base * self.__altura
        return area

    def calcular_perimetro(self):
        perimetro = 2*(self.__base +  self.__altura)
        return perimetro



# r1 = Retangulo(10,5)
# print(r1.calcular_area())
# print(r1.calcular_perimetro())