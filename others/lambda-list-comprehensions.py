# # https://pythonacademy.com.br/blog/list-comprehensions-no-python

numeros = [0,2,3,4,5,7]
pares = []
"""
for numero in numeros:
    if numero % 2 == 0:
        pares.append(numero)
"""
pares = [numero for numero in numeros if numero % 2 == 0]
print(pares)

# ################################################################################
num1 = int(input("Digite um número: "))

s = "par" if num1 % 2 == 0 else "ímpar"
print("O número digitado é ", s)
# ##################################################################################
par = lambda num1: num1 % 2 == 0 
print(par(132))
# ######################################################
resultado = ['1' if numero % 5 == 0 else '0' for numero in range(16)]
print(resultado)

matriz1=[[1,2,3],[3,2,1],[1,3,4]]
matriz2=[[1,2,3],[3,2,1],[1,3,4]]

matriz_res = [[sum([linha[n_col1]*matriz2[n_col1][n_col2] for n_col1 in range(len(matriz1[0]))]) for n_col2 in range(len(matriz2[0]))] for linha in matriz1]

print(matriz_res)
#############################################################
numeros = '2 3 4 5 6 7 8 '.split()

par = lambda numero: numero % 2 == 0
impar = lambda numero: numero % 2 == 1

resultado = [numero for numero in numeros if par(int(numero))]
resultado = [numero for numero in numeros if int(numero) % 2 == 0]
print(resultado)
##################################################################
palavras = 'cachorro gato coelho burro'.split()

procura = lambda palavra, busca: busca == palavra
busca = "burro"
for palavra in palavras:
    print(f'{palavra}: encontrado') if procura(palavra, busca) is True else print(f'{palavra}: não é o que procura')

resultado  = [palavra if procura(palavra, busca) else 'não encontrado' for palavra in palavras]
# resultado  = [palavra for palavra in palavras if procura(palavra, busca) ]
print(resultado)

#######################
from tqdm import tqdm
from time import sleep
print("Arguando tabela consolidada subir para o athena")
for i in tqdm(range(1500)):
    sleep(0.01)
print("Startando pipeline")