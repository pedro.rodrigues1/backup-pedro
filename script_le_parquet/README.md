# Consolida Processos

## O que o script faz:

1) Executa uma query que faz um `SELECT AS` em uma tabela do athena.
2) É gerado um arquivo consolidado com base na query. 
3) Uma tabela no athena é criada olhando para o arquivo consolidado.

Obs: O script consolida os processos através de um SELECT AS de outra tabela do athena, que olha para os processos não consolidados. 


## Como rodar o script:

1) Verificar se tem uma tabela olhando para os arquivos não consolidados.
2) Rodar o script `consolida_processos.py` passando 4 parâmetros no `parameters.yaml` :
* Primeiro: database (database do athena).
* Segundo: table (nome da tabela que irá olhar para os arquivos consolidados).
* Terceiro: reference_table (tabela do athena que olha para os arquivos `não consolidados`).
* Quarto: external_location (locar no s3 onde sera aramazenado os arquivos consolidados).





