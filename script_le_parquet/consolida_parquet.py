import pyarrow.parquet as pq
import s3fs
from dotenv import load_dotenv
import os

load_dotenv()
s3 = s3fs.S3FileSystem(key=os.environ["semantix_access_key"], password=os.environ["semantix_secret_key"])

pandas_dataframe = pq.ParquetDataset('s3://aijus-csn/data_lake/digesto/tribunais/processos/dados_gerais', filesystem=s3).read_pandas().to_pandas()
print(pandas_dataframe)
print(type(pandas_dataframe))

pandas_dataframe.to_parquet("/home/semantix/Downloads/abca.parquet")

