import boto3
from dotenv import load_dotenv
import os
import yaml
import requests
from json import dumps
import datetime
from requests.auth import HTTPBasicAuth
from time import sleep

config = yaml.safe_load(open('parameters.yaml'))
load_dotenv()


database = config['database']
table = config['table']
reference_table = config['reference_table']
external_location = config['external_location']
query= """
CREATE TABLE IF NOT EXISTS %s.%s
WITH (format='PARQUET', external_location='s3:/%s/consolidado') AS
SELECT *
FROM %s; 
"""%(database,table,external_location,reference_table)

index = config['index']
host = config['host']
pipeline_id = config['pipeline_id']
# date = datetime.datetime.today()
# datef = date.strftime('%Y%m%d')
date = "2019-09-19 16:12:03.636146"
print(date)

def run_query(query, database):
    client = boto3.client('athena', region_name='us-east-1', aws_access_key_id=os.environ["semantix_access_key"], aws_secret_access_key= os.environ["semantix_secret_key"])
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': 's3://'+'semantix-aijus-crawler-administrativo/dte_sp' + 'OutputLocation'
        }

    )
    print('Execution ID: ' + response['QueryExecutionId'])
    return response

class StreamsetsWrapper():
    def __init__(self, host):
        self.host = host
          
    def trigger_pipeline_with_parameters(self, database, date, index, table, pipeline_id):
        #sleep para término da tabela criada no athena
        sleep(15)
        url_reset_offset = f"http://{self.host}:18630/rest/v1/pipeline/{pipeline_id}/resetOffset/"
        url_start_pipeline = f"http://{self.host}:18630/rest/v1/pipeline/{pipeline_id}/start/"
   
        payload = {
            'database': database,
            'initial_date': f'{date}',
            'index': index,
            'tabela': table
        }
       
        headers = {
            'X-Requested-By': 'sdc',
            'Content-Type': 'application/json;charset=UTF-8'
        }
        post_1 = requests.post(
            url_reset_offset,
            headers=headers,
            auth=HTTPBasicAuth('admin', 'admin')
        )
        print(post_1)
        post_2 = requests.post(
            url_start_pipeline,
            data= dumps(payload),
            headers=headers,
            auth=HTTPBasicAuth('admin', 'admin')
        )
        print(post_2)

if __name__ == "__main__":
    run_query(query,database)
    print("Aguarde! Criando tabela consolidada")
    StreamsetsWrapper(host).trigger_pipeline_with_parameters(database, date, index, table, pipeline_id)